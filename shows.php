<?php
define('REGEX_SHOW', '/^[a-z0-9_-]+$/');
define('PAGE_SIZE', 50);
define('PAGES_LIMIT', 8);
define('API_URL', 'compress.zlib://https://www.cbs.com/shows/%s/xhr/episodes/page/%d/size/%d/xs/0/season/');

$show = 1 === preg_match(REGEX_SHOW, $_GET['show']) ? $_GET['show'] : NULL;
$ua = isset($_SERVER['HTTP_USER_AGENT']) && "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.0.2 Safari/605.1.15" !== $_SERVER['HTTP_USER_AGENT'] ? $_SERVER['HTTP_USER_AGENT'] : '-';

if (is_null($show)) {
	http_response_code(404);
	exit();
}

$default_opts = array(
	'http' => array(
		'method' => "GET",
		'header' => "Accept: application/json, text/javascript, */*; q=0.01\r\n" .
					"Accept-Encoding: gzip\r\n" .
					"Accept-Language: en-US,en;q=0.5\r\n" .
					"Cache-Control: max-age=0\r\n" .
					"Cookie: CBS_ADV_VAL=c; CBS_ADV_SUBSES_VAL=3\r\n" .
					"DNT: 1\r\n" .
					"User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:66.0) Gecko/20100101 Firefox/66.0\r\n" .
					"X-Requested-With: XMLHttpRequest"
	)
);

$default = stream_context_set_default($default_opts);

function get_results($show, $page=0, $episodes=[]) {

	$api_data = file_get_contents(sprintf(API_URL, $show, $page, PAGE_SIZE), FALSE);

	// On failure return what we have
	if (FALSE === $api_data) {
		return $episodes;
	}

	$json = json_decode($api_data);

	if (FALSE === is_null($json) && property_exists($json, 'result') && property_exists($json->result, 'data')) {

		for ($i=0, $j=sizeof($json->result->data); $i<$j; $i++) {

			$ep = $json->result->data[$i];

			if ($ep->status !== "AVAILABLE" || $ep->type !== "Full Episode") {
				continue;
			}

			$episode = new stdClass;
			$episode->content_id = $ep->content_id;
			$episode->series_title = $ep->series_title;
			$episode->episode_title = $ep->episode_title;
			$episode->airdate = $ep->airdate;
			$episode->season_number = $ep->season_number;
			$episode->episode_number = $ep->episode_number;
			$episode->duration = $ep->duration_raw;
			$episode->thumb = $ep->thumb->large;

			$episodes[] = $episode;
		}

		$max_page = floor($json->result->total/PAGE_SIZE);

		if ($page < $max_page && $page < PAGES_LIMIT) {
			return get_results($show, $page=$page+1, $episodes=$episodes);
		}
	}

	return $episodes;
}

if ('-' !== $ua) {
	$result = get_results($show);
} else {
	$result = [];
}

header('Content-Type: application/json; charset=UTF-8');
echo json_encode($result, JSON_UNESCAPED_SLASHES);
?>